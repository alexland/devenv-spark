#!/bin/bash

HADOOP_V="hadoop-2.7.3"
HADOOP_URL="http://mirror.cogentco.com/pub/apache/hadoop/common/${HADOOP_V}/${HADOOP_V}.tar.gz"


# create data directories for hdfs
mkdir -p var/data/hadoop/hdfs
mkdir -p var/data/hadoop/hdfs/nn
mkdir -p var/data/hadoop/hdfs/snn
mkdir -p var/data/hadoop/hdfs/dn

wget -O hadoop.tar.gz $HADOOP_URL
tar xvf hadoop.tar.gz

# replace config directory
cd $HADOOP_VER/etc
rm -rf hadoop
# and copy the config directory in its place
tar xvf ~/hadoop-config.tar.gz

# format hdfs
~/$HADOOP_VER/bin/hdfs namenode -format

# start services
~/$HADOOP_VER/sbin/start-dfs.sh
~/$HADOOP_VER/sbin/start-yarn.sh

# allow ssh to hadoop (local machine)
ssh-keyscan hadoop >> ~/.ssh/known_hosts


exit 0
