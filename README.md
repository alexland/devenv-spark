



this Vagrantfile installs & configures two VMs:

  - YARN-managed single-node HDFS cluster

  - single-node apache spark cluster

these machines are assigned these private ip addresses:

- `hdfs`: 192.168.33.10
- `spark`: 192.168.33.11

to build these machines:

```shell

  %> git clone git@gitlab.com:alexland/devenv-spark.git

  %> cd devenv-spark

  %> vagrant up

```

HDFS dashboard:

```bash

  %> open -a "Firefox" http://192.168.33.10:8080

```

Spark dashboard:

```bash

  %> open -a "Firefox" http://192.168.33.11:4040

```


note: if you are asked for a password while trying to ssh into a box, and you did not set a password, _vagrant_ is the default pw



If either cluster failed to start--which will be obvious when you try to view
the respective admin panels--you will need to ssh into its machine & restart the daemon, like so:

```bash

vagrant ssh hdfs

# should log in - when logged in run these commands:
./hadoop-2.7.3/sbin/stop-dfs.sh && ./hadoop-2.7.3/sbin/stop-yarn.sh
./hadoop-2.7.3/sbin/start-dfs.sh && ./hadoop-2.7.3/sbin/start-yarn.sh

exit
```

verify that the spark cluster can communicate with the the yarn app (HDFS cluster), ssh into the machine & fire up the spark cluster by starting spark on the driver node, like so:

```bash

%> vagrant ssh spark
%> ./bin/spark-shell


# ./spark-2.0.2-bin-hadoop2.7/bin/spark-submit --master yarn

```

this launches the spark-scala REPL

--------------------


```shell


launching zeppelin

```shell
%> bin/zeppelin-daemon.sh start

%> open -a "Firefox" http://192.168.33.14:8080
```


shutting down zeppelin
```shell
%> bin/zeppelin-daemon.sh stop
```

### helpful vagrant commands

```shell
	# *frequently used* network config for each machine
	%> vagrant ssh-config

	# current machine states
	%> vagrant status

	# for multiple-VM environments, such as this one, add a VM name
	# from the list returned from running the command above,
	# to get more specific information on that VM
	%> vagrant status <VM name>

	# machine states of all active Vagrant environments
	# use --prune to remove invalid entries from the cache
	%> vagrant global-status --prune

	# snapshot & suspend (but do not shut down or destroy)
	%> vagrant suspend

	# compliment to 'vagrant suspend'
	%> vagrant resume

	# shut down the machine Vagrant is managing, -f w/o graceful shutdown
	%> vagrant halt

	# equivalent to 'halt' then 'up'
	%> vagrant reload

	# packages currently running VirtualBox environment
	# as reusable box
	# works only for the providers in default Vagrant
	%> vagrant package

```

### getting help w/ Vagrant

a fast two-step help is to first get a list of all available commands _then_ enter that command with the _-h_ flag to get a one-sentence synopsis, like so:

```shell

	# displays every vagrant command
	%> vagrant

	# displays one-sentence summary of the command entered
	%> vagrant <command> -h

```
